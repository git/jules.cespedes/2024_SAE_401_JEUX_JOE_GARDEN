class Joueur extends Sprite {
  public scene_: Jeu;

  public vitesse_: number;
  public vitessePopUp_: number;

  public score_ : number;
  public monnaie_ : number;

  public timerAnimation: number | null;

  public ecouteurClavier_: any;
  public ecouteurClavierStop_: any;
  private touchesEnfoncees: { [key: string]: boolean };

  public popUpFruit_ : Sprite;
  public popUpPoison_ : Sprite;
  public popUpPlante_ : Sprite;

  public constructor(element: HTMLElement, scene: Jeu) {
    super(element);
    this.scene_ = scene;
    this.timerAnimation = null;

    this.score_ = 0;
    this.monnaie_ = 0;

    this.vitesse_ = 6;
    this.vitessePopUp_ = 15;

    this.touchesEnfoncees = {};
    this.ecouteurClavier_ = (evt: KeyboardEvent) => this.deplacer(evt);
    this.ecouteurClavierStop_ = (evt: KeyboardEvent) => this.stopperDeplacement(evt);

    this.popUpFruit_ = new Sprite (document.getElementById("popupFruit"));
    this.popUpPlante_ = new Sprite (document.getElementById("popupPlante"));
    this.popUpPoison_ = new Sprite (document.getElementById("popupPoison"));

  }

  public collisionObjet() {

    for (let i : number = 0; i<this.scene_.objet_.listeObjets_.length ; i++) {

      if (Joueur.collision(this.getCircle(),this.scene_.objet_.listeObjets_[i].getCircle())) {

            if (this.scene_.objet_.listeObjets_[i] == this.scene_.objet_.recupfruit_) {

              this.scene_.augmenterScore();
              this.popUpFruit();
              this.imagePopUpFruit();
              this.scene_.objet_.listeObjets_[i].setXY(-10000,-10000);
            }

            if (this.scene_.objet_.listeObjets_[i] == this.scene_.objet_.recupPoison_) {

              this.scene_.reduireScore();
              this.popUpPoison();
              this.imagePopUpPoison();

              this.scene_.objet_.listeObjets_[i].setXY(-10000,-10000);
            }

            if (this.scene_.objet_.listeObjets_[i] == this.scene_.objet_.recupPlante_) {

              this.scene_.augmenterMonnaie();
              this.popUpPlante();
              this.imagePopUpPlante();

              this.scene_.objet_.listeObjets_[i].setXY(-10000,-10000);
            }

      }
    }

  }

  public imagePopUpFruit() {
    let imagePopUpFruit_ = new Sprite(document.createElement("img"));

    imagePopUpFruit_.setDimension(16,16);
    imagePopUpFruit_.setImage("img/score.png",24,24);
    imagePopUpFruit_.setX(this.popUpFruit_.getX() + 60);
    imagePopUpFruit_.setY(this.popUpFruit_.getY() + 13);

    this.scene_.appendChild(imagePopUpFruit_);

    setTimeout(() => {
      this.scene_.removeChild(imagePopUpFruit_);
    }, 750);
  }

  public popUpFruit() {

    this.popUpFruit_.getElement().textContent = "+100";
    this.popUpFruit_.setXY(this.getX(),this.getY());

    setTimeout(() => {
      this.popUpFruit_.getElement().textContent = "";
    }, 750);
  }

  public imagePopUpPoison() {
    let imagePopUpPoison_ = new Sprite(document.createElement("img"));

    imagePopUpPoison_.setDimension(16,16);
    imagePopUpPoison_.setImage("img/objet/poisons/poison1.png",24,24);
    imagePopUpPoison_.setX(this.popUpPoison_.getX() + 60);
    imagePopUpPoison_.setY(this.popUpPoison_.getY() + 13);

    this.scene_.appendChild(imagePopUpPoison_);

    setTimeout(() => {
      this.scene_.removeChild(imagePopUpPoison_);
    }, 750);
  }

  public popUpPoison() {

    this.popUpPoison_.getElement().textContent = "-200";
    this.popUpPoison_.setXY(this.getX(),this.getY());

    setTimeout(() => {
      this.popUpPoison_.getElement().textContent = "";
    }, 750);
  }

  public imagePopUpPlante() {
    let imagePopUpPlante_ = new Sprite(document.createElement("img"));

    imagePopUpPlante_.setDimension(16,16);
    imagePopUpPlante_.setImage("img/joePiece.png",24,24);
    imagePopUpPlante_.setX(this.popUpPlante_.getX() + 60);
    imagePopUpPlante_.setY(this.popUpPlante_.getY() + 13);

    this.scene_.appendChild(imagePopUpPlante_);

    setTimeout(() => {
      this.scene_.removeChild(imagePopUpPlante_);
    }, 750);
  }

  public popUpPlante() {

    this.popUpPlante_.getElement().textContent = "+25";
    this.popUpPlante_.setXY(this.getX(),this.getY());

    setTimeout(() => {
      this.popUpPlante_.getElement().textContent = "";
    }, 750);
  }

  public figer() {
    window.removeEventListener("keydown", this.ecouteurClavier_);
    window.removeEventListener("keyup", this.ecouteurClavierStop_);
    clearInterval(this.timerAnimation);
  }

                public animer() {
                  window.addEventListener("keydown", this.ecouteurClavier_);
                  window.addEventListener("keyup", this.ecouteurClavierStop_);
                }

                private deplacementGauche() {
                  this.setX(this.getX() - this.vitesse_);

                }

                private deplacementDroite() {
                  this.setX(this.getX() + this.vitesse_);

                }

                private deplacementHaut() {
                  this.setY(this.getY() - this.vitesse_);

                }

                private deplacementBas() {
                  this.setY(this.getY() + this.vitesse_);

                }

  /*  public deplacer(evt: KeyboardEvent) {
    if (this.timerAnimation !== null) return;
    console.log(evt.key);
    if (evt.key === "ArrowLeft") {
      this.timerAnimation = setInterval(() => this.deplacementGauche(), 1000 / 120);
    }

    else if (evt.key === "ArrowRight") {
      this.timerAnimation = setInterval(() => this.deplacementDroite(), 1000 / 120);
    }

    else if (evt.key === "ArrowUp") {
      this.timerAnimation = setInterval(() => this.deplacementHaut(), 1000 / 120);
    }

    else if (evt.key === "ArrowDown") {
      this.timerAnimation = setInterval(() => this.deplacementBas(), 1000 / 120);
    }
  }*/

  private mettreAJourDeplacement() {

          //Mouvement en diagonales
          if (this.touchesEnfoncees["ArrowLeft"] && this.touchesEnfoncees["ArrowUp"]) {
            this.deplacementGauche();
            this.deplacementHaut();

          }
          else if (this.touchesEnfoncees["ArrowLeft"] && this.touchesEnfoncees["ArrowDown"]) {
            this.deplacementGauche();
            this.deplacementBas();

          }
          else if (this.touchesEnfoncees["ArrowRight"] && this.touchesEnfoncees["ArrowUp"]) {
            this.deplacementDroite();
            this.deplacementHaut();

          }
          else if (this.touchesEnfoncees["ArrowRight"] && this.touchesEnfoncees["ArrowDown"]) {
            this.deplacementDroite();
            this.deplacementBas();


    //Mouvments normaux
    } else if (this.touchesEnfoncees["ArrowLeft"]) {
      this.deplacementGauche();

    } else if (this.touchesEnfoncees["ArrowRight"]) {
      this.deplacementDroite();

    } else if (this.touchesEnfoncees["ArrowUp"]) {
      this.deplacementHaut();

    } else if (this.touchesEnfoncees["ArrowDown"]) {
      this.deplacementBas();

    }
  }

  public deplacer(evt: KeyboardEvent) {
    this.touchesEnfoncees[evt.key] = true;
    if (this.timerAnimation === null) {
      this.timerAnimation = setInterval(() => this.mettreAJourDeplacement(), 1000 / 144);
    }
  }




  //J'ai crée cette fonction avec ChatGPT
  //J'avais trouvé tout seul le moyen d'arrêter mon personnage avec un deuxième écouteur, mais en appuyant sur deux touche simulatement, cela faisait n'importe quoi, j'ai donc rectifié ma fonctione de base qui était celle-ci :
  /*  public nondeplacer(evt : KeyboardEvent) {
    console.log(evt.key);
    if (evt.key == "ArrowLeft") {clearInterval(this.timerAnimation);}
    else if (evt.key == "ArrowRight") {clearInterval(this.timerAnimation);}
    else if (evt.key == "ArrowUp") {clearInterval(this.timerAnimation);}
    else if (evt.key == "ArrowDown") {clearInterval(this.timerAnimation);}
  }*/

  public stopperDeplacement(evt: KeyboardEvent) {
    delete this.touchesEnfoncees[evt.key];
    if (Object.keys(this.touchesEnfoncees).length === 0 && this.timerAnimation !== null) {
      clearInterval(this.timerAnimation);
      this.timerAnimation = null;
    }
  }

  /*public setLimites() {
    this.xmax_ = 0;
    this.xmin_ = 0;
    this.ymax_ = 0;
    this.ymin_ = 0;
  }*/
}
