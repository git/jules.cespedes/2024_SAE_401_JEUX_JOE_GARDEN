//==================================================================================================
// ANIMATION AVEC TYPESCRIPT                                                                 Jeu.ts
//==================================================================================================

// Classe  J e u //---------------------------------------------------------------------------------
class Jeu extends Scene {
 //----------------------------------------------------------------------------------------Attributs
 /* Declarer ici les attributs de la scene. */

 public joueur_ : Joueur;

 public start_ : Start;
 public regles_ : Regles;
 public shop_ : Shop;
 public options_ : Options;


 public objet_ : Objet;

 public intervalFruit: number;
 public intervalPoison: number;
 public intervalPlante: number;
 public intervalCollision: number;
 public intervalTemps: number;

 public compteurScore : number;
 public compteurScoreFinal : number;

 public compteurMonnaie : number;
 public compteurTemps : number;

 public score_ : Sprite;
 public scoreFinal_ : HTMLElement;

 public monnaie_ : Sprite;
 public temps_ : Sprite;

 private zoneDeJeu_ : Sprite;
 public partieFinie_ : HTMLElement;

 //-------------------------------------------------------------------------------------Constructeur
 public constructor(element : HTMLElement) {
  super(element,false);
  /* Ecrire ici le code qui initialise la scene. */


      //Création de la zone de jeu
      this.zoneDeJeu_= new Sprite (document.getElementById("jeu"));
      this.zoneDeJeu_.setWidth(this.getWidth());
      this.zoneDeJeu_.setHeight(this.getHeight());

      //Gestion des stats

      this.score_ = new Sprite (document.getElementById("score"));
      this.compteurScore = 0;

      this.scoreFinal_ = document.getElementById("scoreFinal");

      this.monnaie_ = new Sprite (document.getElementById("monnaie"));
      this.compteurMonnaie = 0;

      this.temps_ = new Sprite (document.getElementById("temps"));
      this.compteurTemps = 90;

      this.partieFinie_ = document.getElementById("partieFinie");

 }

 //--------------------------------------------------------------------------------------------start
 public override start() {
  /* Ecrire ici le code qui demarre la scene. */
  this.start_ = new Start (document.getElementById("starting"), this);
  this.temps();


 }

 public partieCommencer() {

  this.creerJoueur();
  this.afficherRegles();
  this.afficherShop();
  this.afficherOptions();
  this.creerObject();
  this.reduireTemps();



 }

 public creerJoueur() {
  this.joueur_ = new Joueur (document.createElement("img"),this);
  this.joueur_.setImage("img/joe.png",60,80);

  this.joueur_.setX(this.zoneDeJeu_.getWidth()/2 - this.joueur_.getWidth()/2);
  this.joueur_.setY(this.zoneDeJeu_.getHeight()/2 - this.joueur_.getHeight()/2);
  this.appendChild(this.joueur_);
  this.joueur_.animer();
 }

 public afficherRegles() {
  this.regles_ = new Regles(document.getElementById("regles"), this);
 }

 public afficherShop() {
  this.shop_ = new Shop(document.getElementById("Shop"), this);
 }

 public afficherOptions() {
  this.options_ = new Options (document.getElementById("options"), this);
 }

 //Création des objets
 public creerObject() {


  this.objet_ = new Objet (document.createElement("img"),this);
  this.intervalFruit = setInterval(() => {this.objet_.creerFruit();}, 3000);
  this.intervalPoison = setInterval(() => {this.objet_.creerPoison();}, 3000);
  this.intervalPlante = setInterval(() => {this.objet_.creerPlante();}, 3000);

  this.intervalCollision = setInterval(() => {this.joueur_.collisionObjet();}, 1000 / 144);
 }

 public figerObjet() {
  clearInterval(this.intervalFruit);
  clearInterval(this.intervalPoison);
  clearInterval(this.intervalPlante);

  clearInterval(this.intervalCollision);
  clearInterval(this.intervalTemps);

 }

 public augmenterScore() {
  this.compteurScore += 100;
  this.score_.getElement().textContent = "" + this.compteurScore;

}

public reduireScore() {
  this.compteurScore -= 200;
  this.score_.getElement().textContent = "" + this.compteurScore;

}

  public augmenterMonnaie() {
    this.compteurMonnaie +=25;
    this.monnaie_.getElement().textContent = "" + this.compteurMonnaie;

  }

  public temps() {
    this.temps_.getElement().textContent = "" + this.compteurTemps;
  }

  public reduireTemps() {

    this.intervalTemps = setInterval( ()=> {
      this.compteurTemps -=1;
      this.temps_.getElement().textContent = "" + this.compteurTemps;
      if (this.compteurTemps <= 0) {
        this.finDePartie();
      };
    }, 1000 );
  }

  public finDePartie() {
    this.scoreFinal_.textContent = "Votre score est de : " + this.compteurScore;
    this.partieFinie_.style.display = 'block';
    this.figerObjet();
    this.joueur_.figer();

    setTimeout(() => {
      window.location.reload();
    }, 5000);

  }

 //--------------------------------------------------------------------------------------------pause
 public override pause() {
  /* Ecrire ici le code qui met la scene en pause. */
 }

 //------------------------------------------------------------------------------------------unpause
 public override unpause() {
  /* Ecrire ici le code qui sort la scene de la pause. */
 }

 //--------------------------------------------------------------------------------------------clean
 public override clean() {
  /* Ecrire ici le code qui nettoie la scene en vue d'un redemarrage. */
 }
}

// Fin //-------------------------------------------------------------------------------------------
