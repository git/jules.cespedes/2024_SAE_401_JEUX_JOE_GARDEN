class Shop extends Sprite {

  public scene_ : Jeu;

  private menu_ : HTMLElement;
  private fermerImg_: HTMLElement;

  private ouvertureMenu_: HTMLAudioElement;
  private fermetureMenu_: HTMLAudioElement;

  public pelle_ : HTMLElement;
  public arrosoir_ : HTMLElement;


  public constructor(element: HTMLElement, scene: Jeu) {
    super(element);
    this.scene_ = scene;

    this.fermerImg_ = document.getElementById('fermer_');
    this.fermetureMenu_ = document.getElementById('fermerShop') as HTMLAudioElement;
    this.ouvertureMenu_ = document.getElementById('ouvrirShop') as HTMLAudioElement;

    this.pelle_ = document.getElementById('pelle') as HTMLAudioElement;
    this.arrosoir_ = document.getElementById('arrosoir') as HTMLAudioElement;

    this.menu_ = document.getElementById('menu_');
    this.hide();
    this.menu_.style.display = 'block';
    this.clique();

    this.setX(document.documentElement.clientWidth /2 - this.getWidth()/2);
    this.setY(document.documentElement.clientHeight /2 - this.getHeight()/2);
  }

  public clique(): void {
    if (this.fermerImg_) {

        this.fermerImg_.addEventListener('click', this.cacher.bind(this));
        this.menu_.addEventListener('click', this.montrer.bind(this));

        this.pelle_.addEventListener('click', this.choixPelle.bind(this));
        this.arrosoir_.addEventListener('click', this.choixArrosoir.bind(this));
    }
}

  public cacher(): void {
    if (this) {
      this.scene_.creerObject();
      this.scene_.reduireTemps();
      this.fermetureMenu_.play();
        this.hide();
        this.menu_.style.display = 'block';

    }
  }

  public montrer() {
    if (this.menu_) {
      this.scene_.figerObjet();
      this.ouvertureMenu_.play();
      this.show();
      this.menu_.style.display = 'none';
    }
  }

  public choixPelle() {

    if (this.scene_.compteurMonnaie >= 150) {

      this.scene_.compteurMonnaie -= 150;
      this.scene_.monnaie_.getElement().textContent = "" + this.scene_.compteurMonnaie;

      this.scene_.compteurScore += 750;
      this.scene_.score_.getElement().textContent = "" + this.scene_.compteurScore;
      this.scene_.figerObjet();

      this.scene_.creerObject();
      this.scene_.reduireTemps();

  }
}

  public choixArrosoir() {

    if (this.scene_.compteurMonnaie >= 250) {

      this.scene_.joueur_.vitesse_ = 10;
      this.scene_.compteurMonnaie -=250;

      this.scene_.monnaie_.getElement().textContent = "" + this.scene_.compteurMonnaie;
      this.scene_.figerObjet();

      this.scene_.creerObject();
      this.scene_.reduireTemps();

  }
}
}