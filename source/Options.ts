class Options extends Sprite {

  public scene_ : Jeu;

  private menu_ : HTMLElement;

  private fermerImg_: HTMLElement;
  private ouvertureMenu_: HTMLAudioElement;
  private fermetureMenu_: HTMLAudioElement;

  private redemarrer_ : HTMLElement;
  private quitter_ : HTMLElement;


  public constructor(element: HTMLElement, scene: Jeu) {
    super(element);
    this.scene_ = scene;

    this.fermerImg_ = document.getElementById('fermer__');

    this.fermetureMenu_ = document.getElementById('fermerOption') as HTMLAudioElement;
    this.ouvertureMenu_ = document.getElementById('ouvrirOption') as HTMLAudioElement;

    this.redemarrer_ = document.getElementById('recommencer') as HTMLAudioElement;
    this.quitter_ = document.getElementById('quitter') as HTMLAudioElement;

    this.menu_ = document.getElementById('menu__');
    this.hide();
    this.menu_.style.display = 'block';
    this.clique();

    this.setX(document.documentElement.clientWidth /2 - this.getWidth()/2);
    this.setY(document.documentElement.clientHeight /2 - this.getHeight()/2);
  }

  public clique(): void {
    if (this.fermerImg_) {

        this.fermerImg_.addEventListener('click', this.cacher.bind(this));
        this.menu_.addEventListener('click', this.montrer.bind(this));

        this.redemarrer_.addEventListener('click', this.restart.bind(this));
        this.quitter_.addEventListener('click', this.leave.bind(this));
    }
}

public restart() {
  window.location.reload();
}

public leave() {
  window.close();

  this.hide();
}

  public cacher(): void {
    if (this) {
      this.scene_.creerObject();
      this.scene_.reduireTemps();
      this.fermetureMenu_.play();
        this.hide();
        this.menu_.style.display = 'block';

    }
  }

  public montrer() {
    if (this.menu_) {
      clearInterval(this.scene_.intervalTemps);
      this.scene_.figerObjet();
      this.ouvertureMenu_.play();
      this.show();
      this.menu_.style.display = 'none';
    }
  }
}