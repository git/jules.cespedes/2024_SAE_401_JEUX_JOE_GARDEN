class Objet extends Sprite {
  public scene_: Jeu;
  public listeObjets_ : Sprite[];

  public fruit_ : Sprite;
  public Poison_ : Sprite;
  public plante_ : Sprite;

  public recupfruit_ : Sprite;
  public recupPoison_ : Sprite;
  public recupPlante_ : Sprite;

  public limitesX : number;
  public limitesY : number;

  public compteur : number;



  public widthX : number;
  public heightY : number;

  public constructor(element: HTMLElement, scene: Jeu) {
    super(element);
    this.scene_ = scene;

    this.limitesX = 100;
    this.limitesY = 100;
    this.listeObjets_=[];

    this.widthX = this.scene_.getWidth() - this.limitesX;
    this.heightY = this.scene_.getHeight() - this.limitesY;
  }

  //Pour rendre un peu plus vivant le jeu, j'ai demandé à ChatGpt si on pouvait créer une fonction pour pouvoir "set" une image aléatoire pour pour un objet, et ce la fontionnne parfaitement.
  //Cependant comme j'ai plusieurs type d'objet j'ai dû faire plusieurs fonction similaires qui échangent avec les tableaux contenant les différentes images


public imageFruit: string[] = [
  'img/objet/fruits/fraise.png',
  'img/objet/fruits/framboise.png',
  'img/objet/fruits/mure.png',
  'img/objet/fruits/myrtille.png',

];
public imagePoison: string[] = [
  'img/objet/poisons/poison1.png',
  'img/objet/poisons/poison2.png',

];
public imagePlante: string[] = [
  'img/objet/plantes/basilic.png',
  'img/objet/plantes/coriandre.png',
  'img/objet/plantes/melisse.png',
  'img/objet/plantes/origan.png',

];

  public getImageFruit(): string {
  let randomIndex = Math.floor(Math.random() * this.imageFruit.length);
  return this.imageFruit[randomIndex];
}

  public getImagePoison(): string {
  let randomIndex = Math.floor(Math.random() * this.imagePoison.length);
  return this.imagePoison[randomIndex];
}

  public getImagePlante(): string {
  let randomIndex = Math.floor(Math.random() * this.imagePlante.length);
  return this.imagePlante[randomIndex];
  }

  public viderTableau() {
    this.listeObjets_=[];
  }


  public creerFruit() {

    let fruit = new Sprite (document.createElement("img"));
    fruit.disparitionDelay = 2000;

    fruit.setDimension(100,100);
    fruit.getElement().className = "fruits";
    fruit.setImage(this.getImageFruit(),100,100);
    fruit.setXY(this.randomX(this.widthX),this.randomY(this.heightY));

    this.recupfruit_ = fruit;

      this.scene_.appendChild(fruit);
      this.listeObjets_.push(fruit);

    setTimeout(() => {

      for (let i : number = 0; i<this.scene_.objet_.listeObjets_.length ; i++) {
        this.scene_.removeChild(this.listeObjets_[i]);
        this.listeObjets_[i] = null;

    }

     },fruit.disparitionDelay);

  }



  public creerPoison() {

    let poison = new Sprite (document.createElement("img"));
    poison.disparitionDelay = 2000;

    poison.setDimension(100,100);
    poison.getElement().className = "poisons";
    poison.setImage(this.getImagePoison(),75,75);
    poison.setXY(this.randomX(this.widthX),this.randomY(this.heightY));

    this.recupPoison_ = poison;

      this.scene_.appendChild(poison);
      this.listeObjets_.push(poison);

      setTimeout(() => {

        for (let i : number = 0; i<this.scene_.objet_.listeObjets_.length ; i++) {
          this.scene_.removeChild(this.listeObjets_[i]);
          this.listeObjets_[i] = null;
      }

       },poison.disparitionDelay);

  }


  public creerPlante() {


    let plante = new Sprite (document.createElement("img"));
    plante.disparitionDelay = 2000;

    plante.setDimension(100,100);
    plante.getElement().className = "plantes";
    plante.setImage(this.getImagePlante(),100,100);
    plante.setXY(this.randomX(this.widthX),this.randomY(this.heightY));

    this.recupPlante_ = plante;

      this.scene_.appendChild(plante);
      this.listeObjets_.push(plante);


      setTimeout(() => {

        for (let i : number = 0; i<this.scene_.objet_.listeObjets_.length ; i++) {
          this.scene_.removeChild(this.listeObjets_[i]);
          this.listeObjets_[i] = null;
          this.viderTableau();
      }

       },plante.disparitionDelay);
  }





  private randomX(width : number) {
    return Math.floor(Math.random() * width);
  }

  private randomY(height : number) {
    return Math.floor(Math.random() * height);
  }


  //Positionnement aléatoires des objet
  /*  public positionnement() {

    this.listeObjets_.forEach((objet) => {
      objet.setXY(this.randomX(this.widthX), this.randomY(this.heightY));
    });
  }*/

  }