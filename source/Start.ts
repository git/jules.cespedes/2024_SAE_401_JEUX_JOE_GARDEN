class Start extends Sprite {

  public scene_ : Jeu;
  private bouton_ : Sprite;



  private musiqueDeFond : HTMLAudioElement;


  public constructor(element: HTMLElement, scene: Jeu) {
    super(element);
    this.scene_ = scene;

    this.bouton_ = new Sprite (document.getElementById('demarrer'));

    this.musiqueDeFond = document.getElementById('musiqueFond') as HTMLAudioElement;

    this.setX(document.documentElement.clientWidth /2 - this.getWidth()/2);
    this.setY(document.documentElement.clientHeight /2 - this.getHeight()/2);

    this.bouton_.addEventListener('click',this.demarrerPartie.bind(this));
  }

  public demarrerPartie() {
    this.hide();
    this.scene_.partieCommencer();
    this.musiqueDeFond.play();
  }

}