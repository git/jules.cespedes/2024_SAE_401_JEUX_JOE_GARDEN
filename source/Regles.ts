class Regles extends Sprite {
  public scene_ : Jeu;

  private menu_ : HTMLElement;
  private fermerImg_: HTMLElement;
  private son : HTMLElement;
  private sonOui : HTMLElement;

  private ouvertureMenu_: HTMLAudioElement;
  private fermetureMenu_: HTMLAudioElement;

  private musiqueDeFond : HTMLAudioElement;


  public constructor(element: HTMLElement, scene: Jeu) {
    super(element);
    this.scene_ = scene;


    this.fermerImg_ = document.getElementById('fermer');
    this.menu_ = document.getElementById('menu');
    this.son = document.getElementById('son');
    this.sonOui = document.getElementById('sonOui');

    this.fermetureMenu_ = document.getElementById('fermetureMenu') as HTMLAudioElement;
    this.ouvertureMenu_ = document.getElementById('ouvertureMenu') as HTMLAudioElement;
    this.musiqueDeFond = document.getElementById('musiqueFond') as HTMLAudioElement;


    this.hide();
    this.menu_.style.display = 'block';
    this.clique();

    this.setX(document.documentElement.clientWidth /2 - this.getWidth()/2);
    this.setY(document.documentElement.clientHeight /2 - this.getHeight()/2);
  }

  public clique(): void {
    if (this.fermerImg_) {

        this.fermerImg_.addEventListener('click', this.cacher.bind(this));
        this.menu_.addEventListener('click', this.montrer.bind(this));
        this.son.addEventListener('click',this.couper.bind(this));
        this.sonOui.addEventListener('click',this.remettre.bind(this));
    }
}
  public couper() {
    this.musiqueDeFond.pause();
    this.son.style.display = 'none';
    this.sonOui.style.display = 'block';
  }

  public remettre() {
    this.musiqueDeFond.play();
    this.son.style.display = 'block';
    this.sonOui.style.display = 'none';

  }


  public cacher(): void {
    if (this) {
      this.scene_.creerObject();
      this.scene_.reduireTemps();
      this.fermetureMenu_.play();
        this.hide();
        this.menu_.style.display = 'block';

    }
  }

  public montrer() {
    if (this.menu_) {
      this.son.style.display = 'block';
      this.scene_.figerObjet();
      this.ouvertureMenu_.play();
      this.musiqueDeFond.play();
      this.show();
      this.menu_.style.display = 'none';
    }
  }
}