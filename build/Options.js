"use strict";
class Options extends Sprite {
    constructor(element, scene) {
        super(element);
        this.scene_ = scene;
        this.fermerImg_ = document.getElementById('fermer__');
        this.fermetureMenu_ = document.getElementById('fermerOption');
        this.ouvertureMenu_ = document.getElementById('ouvrirOption');
        this.redemarrer_ = document.getElementById('recommencer');
        this.quitter_ = document.getElementById('quitter');
        this.menu_ = document.getElementById('menu__');
        this.hide();
        this.menu_.style.display = 'block';
        this.clique();
        this.setX(document.documentElement.clientWidth / 2 - this.getWidth() / 2);
        this.setY(document.documentElement.clientHeight / 2 - this.getHeight() / 2);
    }
    clique() {
        if (this.fermerImg_) {
            this.fermerImg_.addEventListener('click', this.cacher.bind(this));
            this.menu_.addEventListener('click', this.montrer.bind(this));
            this.redemarrer_.addEventListener('click', this.restart.bind(this));
            this.quitter_.addEventListener('click', this.leave.bind(this));
        }
    }
    restart() {
        window.location.reload();
    }
    leave() {
        window.close();
        this.hide();
    }
    cacher() {
        if (this) {
            this.scene_.creerObject();
            this.scene_.reduireTemps();
            this.fermetureMenu_.play();
            this.hide();
            this.menu_.style.display = 'block';
        }
    }
    montrer() {
        if (this.menu_) {
            clearInterval(this.scene_.intervalTemps);
            this.scene_.figerObjet();
            this.ouvertureMenu_.play();
            this.show();
            this.menu_.style.display = 'none';
        }
    }
}
