"use strict";
class Regles extends Sprite {
    constructor(element, scene) {
        super(element);
        this.scene_ = scene;
        this.fermerImg_ = document.getElementById('fermer');
        this.menu_ = document.getElementById('menu');
        this.son = document.getElementById('son');
        this.sonOui = document.getElementById('sonOui');
        this.fermetureMenu_ = document.getElementById('fermetureMenu');
        this.ouvertureMenu_ = document.getElementById('ouvertureMenu');
        this.musiqueDeFond = document.getElementById('musiqueFond');
        this.hide();
        this.menu_.style.display = 'block';
        this.clique();
        this.setX(document.documentElement.clientWidth / 2 - this.getWidth() / 2);
        this.setY(document.documentElement.clientHeight / 2 - this.getHeight() / 2);
    }
    clique() {
        if (this.fermerImg_) {
            this.fermerImg_.addEventListener('click', this.cacher.bind(this));
            this.menu_.addEventListener('click', this.montrer.bind(this));
            this.son.addEventListener('click', this.couper.bind(this));
            this.sonOui.addEventListener('click', this.remettre.bind(this));
        }
    }
    couper() {
        this.musiqueDeFond.pause();
        this.son.style.display = 'none';
        this.sonOui.style.display = 'block';
    }
    remettre() {
        this.musiqueDeFond.play();
        this.son.style.display = 'block';
        this.sonOui.style.display = 'none';
    }
    cacher() {
        if (this) {
            this.scene_.creerObject();
            this.scene_.reduireTemps();
            this.fermetureMenu_.play();
            this.hide();
            this.menu_.style.display = 'block';
        }
    }
    montrer() {
        if (this.menu_) {
            this.son.style.display = 'block';
            this.scene_.figerObjet();
            this.ouvertureMenu_.play();
            this.musiqueDeFond.play();
            this.show();
            this.menu_.style.display = 'none';
        }
    }
}
