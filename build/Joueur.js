"use strict";
class Joueur extends Sprite {
    constructor(element, scene) {
        super(element);
        this.scene_ = scene;
        this.timerAnimation = null;
        this.score_ = 0;
        this.monnaie_ = 0;
        this.vitesse_ = 6;
        this.vitessePopUp_ = 15;
        this.touchesEnfoncees = {};
        this.ecouteurClavier_ = (evt) => this.deplacer(evt);
        this.ecouteurClavierStop_ = (evt) => this.stopperDeplacement(evt);
        this.popUpFruit_ = new Sprite(document.getElementById("popupFruit"));
        this.popUpPlante_ = new Sprite(document.getElementById("popupPlante"));
        this.popUpPoison_ = new Sprite(document.getElementById("popupPoison"));
    }
    collisionObjet() {
        for (let i = 0; i < this.scene_.objet_.listeObjets_.length; i++) {
            if (Joueur.collision(this.getCircle(), this.scene_.objet_.listeObjets_[i].getCircle())) {
                if (this.scene_.objet_.listeObjets_[i] == this.scene_.objet_.recupfruit_) {
                    this.scene_.augmenterScore();
                    this.popUpFruit();
                    this.imagePopUpFruit();
                    this.scene_.objet_.listeObjets_[i].setXY(-10000, -10000);
                }
                if (this.scene_.objet_.listeObjets_[i] == this.scene_.objet_.recupPoison_) {
                    this.scene_.reduireScore();
                    this.popUpPoison();
                    this.imagePopUpPoison();
                    this.scene_.objet_.listeObjets_[i].setXY(-10000, -10000);
                }
                if (this.scene_.objet_.listeObjets_[i] == this.scene_.objet_.recupPlante_) {
                    this.scene_.augmenterMonnaie();
                    this.popUpPlante();
                    this.imagePopUpPlante();
                    this.scene_.objet_.listeObjets_[i].setXY(-10000, -10000);
                }
            }
        }
    }
    imagePopUpFruit() {
        let imagePopUpFruit_ = new Sprite(document.createElement("img"));
        imagePopUpFruit_.setDimension(16, 16);
        imagePopUpFruit_.setImage("img/score.png", 24, 24);
        imagePopUpFruit_.setX(this.popUpFruit_.getX() + 60);
        imagePopUpFruit_.setY(this.popUpFruit_.getY() + 13);
        this.scene_.appendChild(imagePopUpFruit_);
        setTimeout(() => {
            this.scene_.removeChild(imagePopUpFruit_);
        }, 750);
    }
    popUpFruit() {
        this.popUpFruit_.getElement().textContent = "+100";
        this.popUpFruit_.setXY(this.getX(), this.getY());
        setTimeout(() => {
            this.popUpFruit_.getElement().textContent = "";
        }, 750);
    }
    imagePopUpPoison() {
        let imagePopUpPoison_ = new Sprite(document.createElement("img"));
        imagePopUpPoison_.setDimension(16, 16);
        imagePopUpPoison_.setImage("img/objet/poisons/poison1.png", 24, 24);
        imagePopUpPoison_.setX(this.popUpPoison_.getX() + 60);
        imagePopUpPoison_.setY(this.popUpPoison_.getY() + 13);
        this.scene_.appendChild(imagePopUpPoison_);
        setTimeout(() => {
            this.scene_.removeChild(imagePopUpPoison_);
        }, 750);
    }
    popUpPoison() {
        this.popUpPoison_.getElement().textContent = "-200";
        this.popUpPoison_.setXY(this.getX(), this.getY());
        setTimeout(() => {
            this.popUpPoison_.getElement().textContent = "";
        }, 750);
    }
    imagePopUpPlante() {
        let imagePopUpPlante_ = new Sprite(document.createElement("img"));
        imagePopUpPlante_.setDimension(16, 16);
        imagePopUpPlante_.setImage("img/joePiece.png", 24, 24);
        imagePopUpPlante_.setX(this.popUpPlante_.getX() + 60);
        imagePopUpPlante_.setY(this.popUpPlante_.getY() + 13);
        this.scene_.appendChild(imagePopUpPlante_);
        setTimeout(() => {
            this.scene_.removeChild(imagePopUpPlante_);
        }, 750);
    }
    popUpPlante() {
        this.popUpPlante_.getElement().textContent = "+25";
        this.popUpPlante_.setXY(this.getX(), this.getY());
        setTimeout(() => {
            this.popUpPlante_.getElement().textContent = "";
        }, 750);
    }
    figer() {
        window.removeEventListener("keydown", this.ecouteurClavier_);
        window.removeEventListener("keyup", this.ecouteurClavierStop_);
        clearInterval(this.timerAnimation);
    }
    animer() {
        window.addEventListener("keydown", this.ecouteurClavier_);
        window.addEventListener("keyup", this.ecouteurClavierStop_);
    }
    deplacementGauche() {
        this.setX(this.getX() - this.vitesse_);
    }
    deplacementDroite() {
        this.setX(this.getX() + this.vitesse_);
    }
    deplacementHaut() {
        this.setY(this.getY() - this.vitesse_);
    }
    deplacementBas() {
        this.setY(this.getY() + this.vitesse_);
    }
    mettreAJourDeplacement() {
        if (this.touchesEnfoncees["ArrowLeft"] && this.touchesEnfoncees["ArrowUp"]) {
            this.deplacementGauche();
            this.deplacementHaut();
        }
        else if (this.touchesEnfoncees["ArrowLeft"] && this.touchesEnfoncees["ArrowDown"]) {
            this.deplacementGauche();
            this.deplacementBas();
        }
        else if (this.touchesEnfoncees["ArrowRight"] && this.touchesEnfoncees["ArrowUp"]) {
            this.deplacementDroite();
            this.deplacementHaut();
        }
        else if (this.touchesEnfoncees["ArrowRight"] && this.touchesEnfoncees["ArrowDown"]) {
            this.deplacementDroite();
            this.deplacementBas();
        }
        else if (this.touchesEnfoncees["ArrowLeft"]) {
            this.deplacementGauche();
        }
        else if (this.touchesEnfoncees["ArrowRight"]) {
            this.deplacementDroite();
        }
        else if (this.touchesEnfoncees["ArrowUp"]) {
            this.deplacementHaut();
        }
        else if (this.touchesEnfoncees["ArrowDown"]) {
            this.deplacementBas();
        }
    }
    deplacer(evt) {
        this.touchesEnfoncees[evt.key] = true;
        if (this.timerAnimation === null) {
            this.timerAnimation = setInterval(() => this.mettreAJourDeplacement(), 1000 / 144);
        }
    }
    stopperDeplacement(evt) {
        delete this.touchesEnfoncees[evt.key];
        if (Object.keys(this.touchesEnfoncees).length === 0 && this.timerAnimation !== null) {
            clearInterval(this.timerAnimation);
            this.timerAnimation = null;
        }
    }
}
