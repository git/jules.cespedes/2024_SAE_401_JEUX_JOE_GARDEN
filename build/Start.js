"use strict";
class Start extends Sprite {
    constructor(element, scene) {
        super(element);
        this.scene_ = scene;
        this.bouton_ = new Sprite(document.getElementById('demarrer'));
        this.musiqueDeFond = document.getElementById('musiqueFond');
        this.setX(document.documentElement.clientWidth / 2 - this.getWidth() / 2);
        this.setY(document.documentElement.clientHeight / 2 - this.getHeight() / 2);
        this.bouton_.addEventListener('click', this.demarrerPartie.bind(this));
    }
    demarrerPartie() {
        this.hide();
        this.scene_.partieCommencer();
        this.musiqueDeFond.play();
    }
}
