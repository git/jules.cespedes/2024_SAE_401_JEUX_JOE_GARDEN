"use strict";
class Jeu extends Scene {
    constructor(element) {
        super(element, false);
        this.zoneDeJeu_ = new Sprite(document.getElementById("jeu"));
        this.zoneDeJeu_.setWidth(this.getWidth());
        this.zoneDeJeu_.setHeight(this.getHeight());
        this.score_ = new Sprite(document.getElementById("score"));
        this.compteurScore = 0;
        this.scoreFinal_ = document.getElementById("scoreFinal");
        this.monnaie_ = new Sprite(document.getElementById("monnaie"));
        this.compteurMonnaie = 0;
        this.temps_ = new Sprite(document.getElementById("temps"));
        this.compteurTemps = 90;
        this.partieFinie_ = document.getElementById("partieFinie");
    }
    start() {
        this.start_ = new Start(document.getElementById("starting"), this);
        this.temps();
    }
    partieCommencer() {
        this.creerJoueur();
        this.afficherRegles();
        this.afficherShop();
        this.afficherOptions();
        this.creerObject();
        this.reduireTemps();
    }
    creerJoueur() {
        this.joueur_ = new Joueur(document.createElement("img"), this);
        this.joueur_.setImage("img/joe.png", 60, 80);
        this.joueur_.setX(this.zoneDeJeu_.getWidth() / 2 - this.joueur_.getWidth() / 2);
        this.joueur_.setY(this.zoneDeJeu_.getHeight() / 2 - this.joueur_.getHeight() / 2);
        this.appendChild(this.joueur_);
        this.joueur_.animer();
    }
    afficherRegles() {
        this.regles_ = new Regles(document.getElementById("regles"), this);
    }
    afficherShop() {
        this.shop_ = new Shop(document.getElementById("Shop"), this);
    }
    afficherOptions() {
        this.options_ = new Options(document.getElementById("options"), this);
    }
    creerObject() {
        this.objet_ = new Objet(document.createElement("img"), this);
        this.intervalFruit = setInterval(() => { this.objet_.creerFruit(); }, 3000);
        this.intervalPoison = setInterval(() => { this.objet_.creerPoison(); }, 3000);
        this.intervalPlante = setInterval(() => { this.objet_.creerPlante(); }, 3000);
        this.intervalCollision = setInterval(() => { this.joueur_.collisionObjet(); }, 1000 / 144);
    }
    figerObjet() {
        clearInterval(this.intervalFruit);
        clearInterval(this.intervalPoison);
        clearInterval(this.intervalPlante);
        clearInterval(this.intervalCollision);
        clearInterval(this.intervalTemps);
    }
    augmenterScore() {
        this.compteurScore += 100;
        this.score_.getElement().textContent = "" + this.compteurScore;
    }
    reduireScore() {
        this.compteurScore -= 200;
        this.score_.getElement().textContent = "" + this.compteurScore;
    }
    augmenterMonnaie() {
        this.compteurMonnaie += 25;
        this.monnaie_.getElement().textContent = "" + this.compteurMonnaie;
    }
    temps() {
        this.temps_.getElement().textContent = "" + this.compteurTemps;
    }
    reduireTemps() {
        this.intervalTemps = setInterval(() => {
            this.compteurTemps -= 1;
            this.temps_.getElement().textContent = "" + this.compteurTemps;
            if (this.compteurTemps <= 0) {
                this.finDePartie();
            }
            ;
        }, 1000);
    }
    finDePartie() {
        this.scoreFinal_.textContent = "Votre score est de : " + this.compteurScore;
        this.partieFinie_.style.display = 'block';
        this.figerObjet();
        this.joueur_.figer();
        setTimeout(() => {
            window.location.reload();
        }, 5000);
    }
    pause() {
    }
    unpause() {
    }
    clean() {
    }
}
