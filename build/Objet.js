"use strict";
class Objet extends Sprite {
    constructor(element, scene) {
        super(element);
        this.imageFruit = [
            'img/objet/fruits/fraise.png',
            'img/objet/fruits/framboise.png',
            'img/objet/fruits/mure.png',
            'img/objet/fruits/myrtille.png',
        ];
        this.imagePoison = [
            'img/objet/poisons/poison1.png',
            'img/objet/poisons/poison2.png',
        ];
        this.imagePlante = [
            'img/objet/plantes/basilic.png',
            'img/objet/plantes/coriandre.png',
            'img/objet/plantes/melisse.png',
            'img/objet/plantes/origan.png',
        ];
        this.scene_ = scene;
        this.limitesX = 100;
        this.limitesY = 100;
        this.listeObjets_ = [];
        this.widthX = this.scene_.getWidth() - this.limitesX;
        this.heightY = this.scene_.getHeight() - this.limitesY;
    }
    getImageFruit() {
        let randomIndex = Math.floor(Math.random() * this.imageFruit.length);
        return this.imageFruit[randomIndex];
    }
    getImagePoison() {
        let randomIndex = Math.floor(Math.random() * this.imagePoison.length);
        return this.imagePoison[randomIndex];
    }
    getImagePlante() {
        let randomIndex = Math.floor(Math.random() * this.imagePlante.length);
        return this.imagePlante[randomIndex];
    }
    viderTableau() {
        this.listeObjets_ = [];
    }
    creerFruit() {
        let fruit = new Sprite(document.createElement("img"));
        fruit.disparitionDelay = 2000;
        fruit.setDimension(100, 100);
        fruit.getElement().className = "fruits";
        fruit.setImage(this.getImageFruit(), 100, 100);
        fruit.setXY(this.randomX(this.widthX), this.randomY(this.heightY));
        this.recupfruit_ = fruit;
        this.scene_.appendChild(fruit);
        this.listeObjets_.push(fruit);
        setTimeout(() => {
            for (let i = 0; i < this.scene_.objet_.listeObjets_.length; i++) {
                this.scene_.removeChild(this.listeObjets_[i]);
                this.listeObjets_[i] = null;
            }
        }, fruit.disparitionDelay);
    }
    creerPoison() {
        let poison = new Sprite(document.createElement("img"));
        poison.disparitionDelay = 2000;
        poison.setDimension(100, 100);
        poison.getElement().className = "poisons";
        poison.setImage(this.getImagePoison(), 75, 75);
        poison.setXY(this.randomX(this.widthX), this.randomY(this.heightY));
        this.recupPoison_ = poison;
        this.scene_.appendChild(poison);
        this.listeObjets_.push(poison);
        setTimeout(() => {
            for (let i = 0; i < this.scene_.objet_.listeObjets_.length; i++) {
                this.scene_.removeChild(this.listeObjets_[i]);
                this.listeObjets_[i] = null;
            }
        }, poison.disparitionDelay);
    }
    creerPlante() {
        let plante = new Sprite(document.createElement("img"));
        plante.disparitionDelay = 2000;
        plante.setDimension(100, 100);
        plante.getElement().className = "plantes";
        plante.setImage(this.getImagePlante(), 100, 100);
        plante.setXY(this.randomX(this.widthX), this.randomY(this.heightY));
        this.recupPlante_ = plante;
        this.scene_.appendChild(plante);
        this.listeObjets_.push(plante);
        setTimeout(() => {
            for (let i = 0; i < this.scene_.objet_.listeObjets_.length; i++) {
                this.scene_.removeChild(this.listeObjets_[i]);
                this.listeObjets_[i] = null;
                this.viderTableau();
            }
        }, plante.disparitionDelay);
    }
    randomX(width) {
        return Math.floor(Math.random() * width);
    }
    randomY(height) {
        return Math.floor(Math.random() * height);
    }
}
